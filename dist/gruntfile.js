module.exports = function (grunt){
    grunt.initConfig({
        sass: {
            dist: {
                files:[{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        }

        watch:{
            files: ['*.scss'],
            tasks: ['css']
        },

        browserSync:{
            dev: {
                bsFiles: {//browser files
                    src:[
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server:{
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagenin:{
            dynamic: {
                files: [{
                    expand:true,
                    cwd:'./',
                    src:'',
                    dist:'',
                    
                }]
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-sync');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync' , 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
};